# Guix Packager

This is a small webpage to help you get started with a first basic [Guix recipe](https://guix.gnu.org/manual/devel/en/html_node/Defining-Packages.html).

## Usage

It offers two templates:
  - one based on the "hello" package, using the GNU Autotools build system, and fetching its source with `url-fetch`.
  - one based on a similar package, using the CMake build system, and fetching its source with `git-fetch`.

The purpose is just to give you a working example out of the box, you should definitely feel free to hack around and provide the actual data for your packages!

Since the packages information requires downloading a relatively big amount of data (roughly 5MB), it's not performed automatically, and you have to manually click the "Download packages information" button.
The app will save these information to your browser's local storage so that you don't have to download them every time.

Once you're happy with your recipe you should be able to download the Scheme file, and run `guix shell -f guix-packager.scm` to test your package out.

Currently there is no way to save/import a recipe, if you refresh the page the state of the package will be reset to the default one!

## Note about features

It's worth noting that the purpose of this webpage is *not* to provide a fully featured "recipe editor", but just a basic starting point.

## Source code

It's hosted on [Inria's GitLab](https://gitlab.inria.fr/guix-hpc/guix-packager).
