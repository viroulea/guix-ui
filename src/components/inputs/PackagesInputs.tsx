import { PackagesList, SetState } from "@/lib/types";

import Grid from '@mui/material/Unstable_Grid2';
import LargeOptionsInput from '@/components/inputs/LargeOptionsInput';

type Props = {
  packages: Array<string>,
  inputs: PackagesList,
  setInputs: SetState<PackagesList>,
  nativeInputs: PackagesList,
  setNativeInputs: SetState<PackagesList>,
  propagatedInputs: PackagesList,
  setPropagatedInputs: SetState<PackagesList>,
}
export default function PackagesInputs({
  packages,
  inputs,
  setInputs,
  nativeInputs,
  setNativeInputs,
  propagatedInputs,
  setPropagatedInputs,
}: Props) {
  return (
    <>
      <Grid xs={12}>
        <LargeOptionsInput
          options={packages}
          value={inputs}
          onChange={(event: any, newValue: PackagesList) => setInputs(newValue)}
          label="inputs"
          placeholder="Search packages"
          helperText="Insert packages required to run/build your package."
        />
      </Grid>
      <Grid xs={12}>
        <LargeOptionsInput
          options={packages}
          value={nativeInputs}
          onChange={(event: any, newValue: PackagesList) => setNativeInputs(newValue)}
          label="native-inputs"
          placeholder="Search packages"
          helperText="Insert packages required to build your package."
        />
      </Grid>
      <Grid xs={12}>
        <LargeOptionsInput
          options={packages}
          value={propagatedInputs}
          onChange={(event: any, newValue: PackagesList) => setPropagatedInputs(newValue)}
          label="propagated-inputs"
          placeholder="Search packages"
          helperText="Insert packages that needs to be included when depending on your package."
        />
      </Grid>
    </>
  )
}
