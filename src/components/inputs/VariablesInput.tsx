import {
  ChangeEvent,
  SetState
} from '@/lib/types';
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import FormControl from '@mui/material/FormControl';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

import { useCallback } from 'react';

type Props = {
  variables: Array<string>,
  setVariables: SetState<Array<string>>,
}

export default function VariablesInput({
  variables,
  setVariables,
}: Props) {
  const onVariableChange = useCallback((event: ChangeEvent, index: number) =>
    setVariables(currentVariables => {
      const newVariables = [...currentVariables];
      newVariables[index] = event.target.value;
      return newVariables;
    }), [setVariables]);

  const addVariable = useCallback(() =>
    setVariables((currentVariables) => {
      return [...currentVariables, ""]
    }), [setVariables]);

  const removeVariable = useCallback((index: number) => setVariables(currentVariables =>
    currentVariables.slice(0, index).concat(currentVariables.slice(index + 1, currentVariables.length))
  ), [setVariables]);

  return (
    <>
      <Typography variant="h6" sx={{mb: 2}}>
          Configuration variables
      </Typography>
      {variables.map((v, index) => (
        <FormControl fullWidth variant="standard" sx={{mb: 2}} key={index}>
          <TextField
            label={`Variable ${index}`}
            value={v}
            onChange={(event: ChangeEvent) => onVariableChange(event, index)}
            InputProps={{
              endAdornment: <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => removeVariable(index)}
                  edge="end"
                >
                  <CloseIcon />
                </IconButton>
              </InputAdornment>
            }}
          />
        </FormControl>
      ))}
      <Button
        fullWidth
        variant="contained"
        color="success"
        onClick={addVariable}
        sx={{mb: 2}}
      >
            Add variable
      </Button>
    </>
  );
}
