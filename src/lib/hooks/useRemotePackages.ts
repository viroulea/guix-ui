import { useCallback, useEffect, useState } from 'react';

export function useCachedData<T>(key: string, url: string, defaultValue: T, processData: (data: T) => T = (data: T) => data) {
  const [data, setData] = useState<T>(defaultValue);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState(null);

  const fetchData = useCallback(() => {
    setLoading(true);
    setError(null);
    fetch(url).then(response => {
      const { ok, status } = response;
      if (ok) {
        return response.json()
      }
      throw new Error(`Something went wrong and server returned status ${response.status}`);
    }).then(json => {
      const data = processData(json);
      setData(data);
      try {
        localStorage.setItem(key, JSON.stringify(data));
      } catch {
        // setItem may fail if the localStorage doesn't have enough space
        // (roughly 3MB when writing this), just catch the error and do nothing
        // since we simply won't use localStorage in this case.
      }
    }).catch(err => setError(err.message))
      .finally(() => setLoading(false));
  }, [key, url, processData, setData, setError, setLoading]);

  useEffect(() => {
    const storedData = JSON.parse(localStorage.getItem(key) || 'null');
    if (storedData) {
      setData(storedData);
    }
    setLoading(false);
  }, [key]);

  const resetAndFetchData = useCallback(() => {
    localStorage.removeItem(key);
    fetchData();
  }, [key, fetchData]);

  return { data, resetAndFetchData, loading, error };
}
